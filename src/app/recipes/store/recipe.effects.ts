import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as RecipeActions from './recipe.actions';
import { Recipe } from '../recipe.model';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducer';

@Injectable() // senza questo non si possono usare le dependency injection nel Effect
export class RecipeEffects {
  @Effect()
  // ofType è uno speciale operatore che specifica per quali azioni si continua nella catena di esecuzione
  fetchRecipes = this.$actions.pipe(
    ofType<RecipeActions.FetchRecipes>(RecipeActions.FETCH_RECIPES),
    switchMap(() => {
      return this.http.get<Recipe[]>(
        'https://ng-recipe-book-64962.firebaseio.com/recipes.json'
      );
    }),
    map((recipes) => {
      return recipes.map((recipe) => {
        return {
          ...recipe,
          ingredients: recipe.ingredients ? recipe.ingredients : [],
        };
      });
    }),
    map((recipes) => {
      return new RecipeActions.SetRecipes(recipes); // dispatch action SetRecipe
    })
  );

  @Effect({ dispatch: false }) // non si fa il dispatch di nessuna azione
  storeRecipes = this.$actions.pipe(
    ofType<RecipeActions.StoreRecipe>(RecipeActions.STORE_RECIPES),
    withLatestFrom(this.store.select('recipes')), // l'ultimo valore emesso da un altro observable
    switchMap(([actionData, recipesState]) => {
      return this.http.put(
        'https://ng-recipe-book-64962.firebaseio.com/recipes.json',
        recipesState.recipes
      );
    })
  );

  constructor(
    private $actions: Actions,
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) {}
}
