import { Recipe } from '../recipe.model';
import * as RecipesActions from './recipe.actions';

export interface State {
  recipes: Recipe[];
}

const initialState: State = {
  recipes: []
};

export function recipeReducer(
  state = initialState,
  action: RecipesActions.RecipesActions
) {
  switch (action.type) {
    case RecipesActions.SET_RECIPES:
      return {
        ...state,
        recipes: [...action.payload]
      };
    case RecipesActions.ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload] // tutte le vecchie ricette più qualla aggiunta
      };
    case RecipesActions.UPDATE_RECIPE:
      // Si sostutuisce alla copia della vecchia ricetta quella nuova
      const updatedRecipe = {
        ...state.recipes[action.payload.index], // ricetta da modificare
        ...action.payload.newRecipe // nuova ricetta
      };

      const updatedRecipes = [...state.recipes]; // array di ricette
      updatedRecipes[action.payload.index] = updatedRecipe;

      return {
        ...state,
        recipes: updatedRecipes
      };
    case RecipesActions.DELETE_RECIPE:
      return {
        ...state,
        recipes: state.recipes.filter((recipe, index) => { // restituisco tutte le ricette eslusa quella per specifico indice
          return index !== action.payload;
        })
      };
    default:
      return state;
  }
}
