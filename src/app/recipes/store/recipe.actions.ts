import { Action } from '@ngrx/store';
import { Recipe } from '../recipe.model';

export const SET_RECIPES = '[Recipes] Set Recipes';
export const FETCH_RECIPES = '[Recipes] Fetch Recipes';
export const ADD_RECIPE = '[Recipe] Add Recipe';
export const UPDATE_RECIPE = '[Recipe] Update Recipe';
export const DELETE_RECIPE = '[Recipe] Delete Recipe';
export const STORE_RECIPES = '[Recipe] Store Recipes';

// tslint:disable-next-line: class-name
export class SetRecipes implements Action {
  readonly type = SET_RECIPES;

  constructor(public payload: Recipe[]) {}
}

// tslint:disable-next-line: class-name
export class FetchRecipes implements Action {
  readonly type = FETCH_RECIPES;
}

// tslint:disable-next-line: class-name
export class AddRecipe implements Action {
  readonly type = ADD_RECIPE;

  constructor(public payload: Recipe) {}
}

// tslint:disable-next-line: class-name
export class UpdateRecipe implements Action {
  readonly type = UPDATE_RECIPE;

  constructor(
    public payload: {
      index: number;
      newRecipe: Recipe;
    }
  ) {}
}
// tslint:disable-next-line: class-name
export class DeleteRecipe implements Action {
  readonly type = DELETE_RECIPE;

  constructor(public payload: number) {}
}

// tslint:disable-next-line: class-name
export class StoreRecipe implements Action {
  readonly type = STORE_RECIPES;
}

export type RecipesActions =
  | SetRecipes
  | FetchRecipes
  | AddRecipe
  | UpdateRecipe
  | DeleteRecipe
  | StoreRecipe;
